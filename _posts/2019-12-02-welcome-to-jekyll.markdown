---
layout: post
title:  "Antonio Gala - Bajo los fuegos de fugaces colores"
date:   2019-12-02 19:55:10 +0100
categories: poemas
---
## Bajo los fuegos de fugaces colores ##

Bajo los fuegos de fugaces colores  
que iluminan el aire de la noche,  
dame tu mano.  
Mira abrirse las palmeras doradas, rojas, verdes;  
caen los frutos azules de la altura;  
rasgan el negro terciopelo  
las estelas de plata…  
En tus ojos yo veo el frío ardor,  
artificial y efímero  
de los castillos que veloces surgen  
y veloces se extinguen.  
Dame tu mano: es todo cuanto tengo  
en medio de esta falsa  
riqueza, de esta dádiva  
que fugazmente se otorga y se consume.  
Así es todo: organizado y yerto  
brota el amor, crece, se desparrama, se hunde,  
vuelve la oscuridad  
en la que, previsto y bien envuelto, yacía.  
Nada, nada…  
Dame tu mano. Entre los irisados estampidos  
alegres sólo para los alegres,  
se esfuma el corazón, igual que una girándula  
demasiado mojada para arder o dar luz.  
En este tornasolado e intrincado bosque  
dame tu mano para que no me pierda.  
